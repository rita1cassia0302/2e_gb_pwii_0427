import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadLivroRoutingModule } from './cad-livro-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadLivroRoutingModule
  ]
})
export class CadLivroModule { }
